package firstnonrepeatcharacter

import (
	"strings"
)

// package main

func FirstNonRepeating(str string) string {
	if str == "" {
		return ""
	}
	seen := make(map[rune]int)
	position := make(map[int]rune)
	seenLower := make(map[string]int)

	for i, r := range str {
		elem, ok := seen[r]
		elemStr, okLower := seenLower[strings.ToLower(string(r))]
		if ok {
			delete(position, elem)
			continue
		}
		if okLower {
			delete(position, elemStr)
			continue
		}

		seen[r] = i
		position[i] = r
		seenLower[strings.ToLower(string(r))] = i
	}
	least := len(str)
	for i := range position {
		if i < least {
			least = i
		}
	}
	if len(position) == 0 {
		return ""
	}
	return string(position[least])
}

// func main() {
// 	fmt.Println(FirstNonRepeating("stress"))
// }
