package firstnonrepeatcharacter_test

import (
	"testing"

	fst "gitlab.com/utt_meelis/codewars/FirstNonRepeatCharacter"
)

func TestFirstNonRepeating(t *testing.T) {
	if result := fst.FirstNonRepeating("a"); result != "a" {
		t.Fatalf("Excpected %v, got %v\n", "a", result)
	}
	if result := fst.FirstNonRepeating("stress"); result != "t" {
		t.Fatalf("Excpected %v, got %v\n", "t", result)
	}
	if result := fst.FirstNonRepeating("moonmen"); result != "e" {
		t.Fatalf("Excpected %v, got %v\n", "e", result)
	}
	if result := fst.FirstNonRepeating(""); result != "" {
		t.Fatalf("Excpected %v, got %v\n", "", result)
	}
	if result := fst.FirstNonRepeating("abba"); result != "" {
		t.Fatalf("Excpected %v, got %v\n", "", result)
	}
	if result := fst.FirstNonRepeating("aa"); result != "" {
		t.Fatalf("Excpected %v, got %v\n", "", result)
	}
	if result := fst.FirstNonRepeating("~><#~><"); result != "#" {
		t.Fatalf("Excpected %v, got %v\n", "#", result)
	}
	if result := fst.FirstNonRepeating("hello world, eh?"); result != "w" {
		t.Fatalf("Excpected %v, got %v\n", "w", result)
	}
	if result := fst.FirstNonRepeating("sTreSS"); result != "T" {
		t.Fatalf("Excpected %v, got %v\n", "T", result)
	}
	if result := fst.FirstNonRepeating("Go hang a salami, I'm a lasagna hog!"); result != "," {
		t.Fatalf("Excpected %v, got %v\n", ",", result)
	}
	if result := fst.FirstNonRepeating("o.dIekEGJQAvBvhS0bXsEjEX6Avb"); result != "o" {
		t.Fatalf("Excpected %v, got %v\n", "o", result)
	}

}
