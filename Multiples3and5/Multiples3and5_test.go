package multiples3and5_test

import (
	"testing"

	multi "gitlab.com/utt_meelis/codewars/Multiples3and5"
)

func TestMultiples3and5(t *testing.T) {
	if result := multi.Multiple3And5(10); result != 23 {
		t.Fatalf("Expected %v, got %v\n", 23, result)
	}
	if result := multi.Multiple3And5(34); result != 258 {
		t.Fatalf("Expected %v, got %v\n", 258, result)
	}

	if result := multi.Multiple3And5(34); result != 258 {
		t.Fatalf("Expected %v, got %v\n", 258, result)
	}

	if result := multi.Multiple3And5(-10); result != 0 {
		t.Fatalf("Expected %v, got %v\n", 0, result)
	}
}
