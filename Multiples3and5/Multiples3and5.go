package multiples3and5

func iter(number int, multi int, count int) (result int) {
	for i := 1; i <= count; i++ {
		if inter := multi * i; inter < number {
			if multi == 3 && inter%5 == 0 {
				continue
			}
			result += inter
		}
	}
	return result
}

func Multiple3And5(number int) int {
	if number < 0 {
		return 0
	}
	return iter(number, 3, number/3) + iter(number, 5, number/5)
}
